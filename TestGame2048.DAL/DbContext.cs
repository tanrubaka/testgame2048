﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using TestGame2048.Configuration;
using TestGame2048.Models.DomainObjects;

namespace TestGame2048.DAL
{
    public class DbContext
    {
        private readonly IMongoDatabase _database;

        private IMongoCollection<Game> Games => _database.GetCollection<Game>(nameof(Games));

        public DbContext(IAppConfig appConfig)
        {
            var connection = new MongoUrlBuilder(appConfig.ConnectionString);

            MongoClient client = new MongoClient(appConfig.ConnectionString);

            _database = client.GetDatabase(connection.DatabaseName);
        }
        
        public async Task<Game> GetGame(Guid id)
        {
            return await Games.Find(new BsonDocument("_id", id)).FirstOrDefaultAsync();
        }
        
        public async Task Create(Game model)
        {
            if (model.Id == Guid.Empty)
            {
                model.Id = Guid.NewGuid();
            }
            model.CreateDate = DateTime.UtcNow;
            
            await Games.InsertOneAsync(model);
        }

        public async Task Update(Game model)
        {
            await Games.ReplaceOneAsync(new BsonDocument("_id", model.Id), model);
        }

        public async Task Remove(Guid id)
        {
            await Games.DeleteOneAsync(new BsonDocument("_id", id));
        }

        
    }
}

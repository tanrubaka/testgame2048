﻿using Microsoft.Extensions.DependencyInjection;

namespace TestGame2048.DAL
{
    public static class Initiation
    {
        public static void Init(IServiceCollection services)
        {
            services.AddTransient<DbContext>();
        }
    }
}
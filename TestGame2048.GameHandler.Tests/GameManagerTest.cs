using System;
using System.IO;
using System.Linq;
using Moq;
using Newtonsoft.Json;
using TestGame2048.Configuration;
using TestGame2048.GameHandler.Enums;
using TestGame2048.GameHandler.Managers;
using TestGame2048.Models.DomainObjects;
using Xunit;

namespace TestGame2048.GameHandler.Tests
{
    public class GameManagerTest
    {
        [Fact]
        public void StartGame()
        {
            var appConfig = new Mock<IAppConfig>();
            appConfig.Setup(i => i.GridSize).Returns(4);
            appConfig.Setup(i => i.StartTiles).Returns(2);
            var gameManager = new GameManager(appConfig.Object);
            var game = gameManager.Start();
            Assert.Equal(CountTiles(game.Grid), 2);
        }

        private int CountTiles(Grid grid)
        {
            var tilesCount = 0;
            for (int x = 0; x < grid.Size; x++)
            {
                for (int y = 0; y < grid.Size; y++)
                {
                    if (grid.Tiles[x, y] != null)
                    {
                        tilesCount++;
                    }
                }
            }
            return tilesCount;
        }

        [Fact]
        public void Move()
        {
            var appConfig = new Mock<IAppConfig>();
            appConfig.Setup(i => i.GridSize).Returns(4);
            appConfig.Setup(i => i.StartTiles).Returns(2);
            var gameManager = new GameManager(appConfig.Object);
            
            var sourceGame = JsonConvert.DeserializeObject<Game>(GetJson("GameTemplate/sourceGame.json"));
            var game = gameManager.Move(sourceGame, Direction.Right);
            var resultGame = JsonConvert.DeserializeObject<Game>(GetJson("GameTemplate/resultGame.json"));

            Assert.Equal(game.Score, resultGame.Score);
        }

        [Fact]
        public void Finish()
        {
            var appConfig = new Mock<IAppConfig>();
            appConfig.Setup(i => i.GridSize).Returns(4);
            appConfig.Setup(i => i.StartTiles).Returns(2);
            var gameManager = new GameManager(appConfig.Object);

            var finishGame = JsonConvert.DeserializeObject<Game>(GetJson("GameTemplate/finishGame.json"));
            var game = gameManager.Move(finishGame, Direction.Right);
            Assert.Equal(game.Won, true);
        }
        
        private string GetJson(string filePath)
        {
            FileStream fileStream = new FileStream(filePath, FileMode.Open);
            using (StreamReader reader = new StreamReader(fileStream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}

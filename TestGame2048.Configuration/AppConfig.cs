﻿using System;
using Microsoft.Extensions.Configuration;

namespace TestGame2048.Configuration
{
    public class AppConfig: IAppConfig
    {
        public IConfiguration Configuration { get; set; }

        public string ConnectionString => Configuration["ConnectionStrings:DataAccessMongoProvider"];

        public int GridSize
        {
            get
            {
                if (int.TryParse(Configuration["Game:GridSize"], out var value))
                {
                    return value;
                }
                throw new InvalidOperationException(Resources.Resource.ErrorGridSize);
            }
        }

        public int StartTiles {
            get
            {
                if (int.TryParse(Configuration["Game:StartTiles"], out var value))
                {
                    return value;
                }
                throw new InvalidOperationException(Resources.Resource.ErrorStartTiles);
            }
        }

        public AppConfig(IConfiguration configuration)
        {
            Configuration = configuration;
        }
    }
}

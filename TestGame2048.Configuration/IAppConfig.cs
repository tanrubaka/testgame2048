﻿namespace TestGame2048.Configuration
{
    public interface IAppConfig
    {
        string ConnectionString { get; }
        int GridSize { get; }
        int StartTiles { get; }
    }
}
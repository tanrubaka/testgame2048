﻿using System;
using TestGame2048.GameHandler.Enums;

namespace TestGame2048.Model
{
    public class MoveOption
    {
        public Guid GameId { get; set; }
        public Direction Direction { get; set; }
    }
}
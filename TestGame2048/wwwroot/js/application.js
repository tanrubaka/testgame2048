
window.requestAnimationFrame(function () {
    new GameManager(KeyboardInputManager, HTMLActuator);
});

var game;
var inputManager;
var actuator;
var isLoad;

function GameManager(InputManager, Actuator) {
    inputManager = new InputManager;
    actuator = new Actuator;

    inputManager.on("move", this.move.bind(this));
    inputManager.on("restart", this.restart.bind(this));
}


function actuate() {
    actuator.actuate(game.grid, {
        score: game.score,
        over: game.over,
        won: game.won,
        bestScore: game.bestScore
    });

};

GameManager.prototype.restart = function () {
    if (isLoad) {
        return;
    }
    isLoad = true;
    var url = '/api/startgame';
    var $errors = $('#errors');
    $errors.text("");
    $.ajax({
        url: url,
        dataType: 'json',
        type: 'POST',
        data:"",
        success: function (result) {
            game = result;
            actuate();
            isLoad = false;
        },
        error: (function (xhr, status) {
            if (xhr.responseJSON.error) {
                $errors.append(xhr.responseJSON.error + "<br/>");
            }
            isLoad = false;
        })
    });
}

GameManager.prototype.move = function (direction) {
    if (isLoad) {
        return;
    }
    isLoad = true;
    var url = '/api/move';
    var $errors = $('#errors');
    $errors.text("");
    var option = {
        gameId: game.id,
        direction: direction
    };
    $.ajax({
        url: url,
        dataType: 'json',
        contentType: "application/json",
        type: 'POST',
        data: JSON.stringify(option),
        success: function (result) {
            game = result;
            actuate();
            isLoad = false;
        },
        error: (function (xhr, status) {
            if (xhr.responseJSON.error) {
                $errors.append(xhr.responseJSON.error + "<br/>");
            }
            isLoad = false;
        })
    });
}
﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestGame2048.DAL;
using TestGame2048.GameHandler.IManagers;
using TestGame2048.Models.DomainObjects;

namespace TestGame2048.Controllers
{
    [Produces("application/json")]
    [Route("api/StartGame")]
    public class StartGameController : Controller
    {
        private readonly IGameManager _gameManager;
        private readonly DbContext _dbContext;

        public StartGameController(IGameManager gameManager,
            DbContext context)
        {
            _gameManager = gameManager;
            _dbContext = context;
        }

        [HttpPost]
        public async Task<Game> Post()
        {
            var game = _gameManager.Start();
            await _dbContext.Create(game);
            return game;
        }
    }
}
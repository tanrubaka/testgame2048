﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestGame2048.DAL;
using TestGame2048.GameHandler.IManagers;
using TestGame2048.Model;
using TestGame2048.Models.DomainObjects;

namespace TestGame2048.Controllers
{
    [Produces("application/json")]
    [Route("api/Move")]
    public class MoveController : Controller
    {
        private readonly IGameManager _gameManager;
        private readonly DbContext _dbContext;

        public MoveController(IGameManager gameManager,
            DbContext context)
        {
            _gameManager = gameManager;
            _dbContext = context;
        }

        [HttpPost]
        public async Task<Game> Post(
            [FromBody]MoveOption option
            )
        {
            var previousState =  await _dbContext.GetGame(option.GameId);
            if (previousState==null)
            {
                throw new Exception("Your game was not found");
            }
            var game = _gameManager.Move(previousState, option.Direction);
            await _dbContext.Update(game);
            return game;
        }
    }
}
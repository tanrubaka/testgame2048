﻿using System.Collections.Generic;

namespace TestGame2048.Models.DomainObjects
{
    public class Tile
    {
        public Position Position { get; set; }
        public int Value { get; set; }

        public List<Tile> MergedFrom { get; set; }
        
        public Tile(Position position, int? value)
        {
            this.Position = new Position
            {
                X = position.X,
                Y = position.Y
            };
            this.Value = value ?? 2;
        }
    }
}
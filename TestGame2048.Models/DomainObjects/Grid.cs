﻿namespace TestGame2048.Models.DomainObjects
{
    public class Grid
    {
        public int Size { get; set; }
        public Tile[,] Tiles { get; set; }

        public Grid(int size)
        {
            this.Tiles = new Tile[size, size];
            this.Size = size;
        }
    }
}
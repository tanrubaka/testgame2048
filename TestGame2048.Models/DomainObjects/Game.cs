﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace TestGame2048.Models.DomainObjects
{
    public class Game
    {
        [BsonId]
        public Guid Id { get; set; }
        public Grid Grid { get; set; }
        public int Score { get; set; }
        public int BestScore { get; set; }
        public bool Over { get; set; }
        public bool Won { get; set; }
        public bool KeepPlaying { get; set; }

        public DateTime CreateDate { get; set; }

        public Game(int gridSize)
        {
            this.Grid = new Grid(gridSize);
        }
    }
}
﻿namespace TestGame2048.Models.DomainObjects
{
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
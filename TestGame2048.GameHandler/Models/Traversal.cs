﻿namespace TestGame2048.GameHandler.Models
{
    public class Traversal
    {
        public int[] X { get; set; }
        public int[] Y { get; set; }

        public Traversal(int size)
        {
            this.X = new int[size];
            this.Y = new int[size];
        }
    }
}
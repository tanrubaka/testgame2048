﻿using TestGame2048.Models.DomainObjects;

namespace TestGame2048.GameHandler.Models
{
    public class FarthestPosition
    {
        public Position Farthest { get; set; }
        public Position Next { get; set; }
    }
}
﻿using TestGame2048.GameHandler.Enums;
using TestGame2048.Models.DomainObjects;

namespace TestGame2048.GameHandler.IManagers
{
    public interface IGameManager
    {
        Game Start();
        Game Move(Game game, Direction direction);
    }
}
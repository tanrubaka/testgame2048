﻿using Microsoft.Extensions.DependencyInjection;
using TestGame2048.GameHandler.IManagers;
using TestGame2048.GameHandler.Managers;

namespace TestGame2048.GameHandler
{
    public static class Initiation
    {
        public static void Init(IServiceCollection services)
        {
            services.AddTransient<IGameManager, GameManager>();
        }
    }
}
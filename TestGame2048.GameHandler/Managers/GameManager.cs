﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestGame2048.GameHandler.Enums;
using TestGame2048.GameHandler.Extensions;
using TestGame2048.GameHandler.IManagers;
using TestGame2048.GameHandler.Models;
using TestGame2048.Models.DomainObjects;
using TestGame2048.Configuration;

namespace TestGame2048.GameHandler.Managers
{
    public class GameManager: IGameManager
    {
        private readonly IAppConfig _appConfig;

        public GameManager(IAppConfig appConfig)
        {
            _appConfig = appConfig;
        }

        public Game Start()
        {
            var game = new Game(_appConfig.GridSize);
            AddStartTiles(game);
            return game;
        }

        public Game Move(Game game, Direction direction)
        {
            if (IsGameTerminated(game))
            {
                return game;
            }

            var vector = GetVector(direction);
            var traversals = BuildTraversals(vector);
            var moved = false;

            game.Grid.PrepareTiles();

            foreach (var x in traversals.X)
            {
                foreach (var y in traversals.Y)
                {
                    var cell = new Position { X = x, Y = y };
                    var tile = game.Grid.Tiles[x, y];

                    if (tile != null)
                    {
                        var positions = game.Grid.FindFarthestPosition(cell, vector);
                        var next = game.Grid.CellContent(positions.Next);

                        if (next != null && next.Value == tile.Value &&
                            (next.MergedFrom == null || !next.MergedFrom.Any()))
                        {
                            var merged =
                                new Tile(positions.Next, tile.Value * 2)
                                {
                                    MergedFrom = new List<Tile> { tile, next }
                                };

                            game.Grid.Tiles[merged.Position.X, merged.Position.Y] = merged;
                            game.Grid.Tiles[tile.Position.X, tile.Position.Y] = null;

                            tile.Position.X = positions.Next.X;
                            tile.Position.Y = positions.Next.Y;

                            game.Score += merged.Value;

                            if (merged.Value == 2048) game.Won = true;
                        }
                        else
                        {
                            MoveTile(game, tile, positions.Farthest);
                        }

                        if (!PositionsEqual(cell, tile.Position))
                        {
                            moved = true;
                        }
                    }
                }
            }
            if (moved)
            {
                var tile = AddRandomTile(game);
                if (tile != null)
                {
                    game.Grid.Tiles[tile.Position.X, tile.Position.Y] = tile;
                }
                if (!MovesAvailable(game))
                {
                    game.Over = true;
                }
                Actuate(game);
            }
            return game;
        }

        private void AddStartTiles(Game game)
        {
            for (int i = 0; i < _appConfig.StartTiles; i++)
            {
                var tile = AddRandomTile(game);
                if (tile!=null)
                {
                    game.Grid.Tiles[tile.Position.X, tile.Position.Y] = tile;
                }
            }
        }

        private Tile AddRandomTile(Game game)
        {
            var ramdom = new Random();
            var value = ramdom.NextDouble() < 0.9 ? 2 : 4;
            var porition = RandomAvailableCell(game);
            return porition==null ? null : new Tile(porition, value);
        }

        private Position RandomAvailableCell(Game game)
        {
            var cells = game.Grid.AvailableCells();
            if (cells.Any())
            {
                var ramdom = new Random();
                return cells[ramdom.Next(cells.Count)];
            }
            return null;
        }
        
        private bool IsGameTerminated(Game game)
        {
            return game.Over || (game.Won && !game.KeepPlaying);
        }

        private Position GetVector(Direction direction)
        {
            var map = new List<Position>{
                new Position { X= 0,  Y= -1 }, // Up
                new Position { X= 1,  Y= 0 },  // Right
                new Position { X= 0,  Y= 1 },  // Down
                new Position { X= -1, Y= 0 }   // Left
            };

            return map[(int)direction];
        }

        private Traversal BuildTraversals(Position vector)
        {
            var traversals = new Traversal(_appConfig.GridSize);

            for (var pos = 0; pos< _appConfig.GridSize; pos++) {
                traversals.X[pos]=pos;
                traversals.Y[pos]=pos;
            }
            
            if (vector.X == 1) Array.Reverse(traversals.X);
            if (vector.Y == 1) Array.Reverse(traversals.Y);

            return traversals;
        }

        private void MoveTile(Game game,Tile tile, Position cell)
        {
            game.Grid.Tiles[tile.Position.X, tile.Position.Y] = null;
            game.Grid.Tiles[cell.X, cell.Y] = tile;
            tile.Position.X = cell.X;
            tile.Position.Y = cell.Y;
        }

        private bool PositionsEqual(Position first, Position second)
        {
            return first.X == second.X && first.Y == second.Y;
        }

        private void Actuate(Game game)
        {
            if (game.BestScore < game.Score)
            {
                game.BestScore = game.Score;
            }
        }

        private bool MovesAvailable (Game game) {
            return game.Grid.AvailableCells().Any() || TileMatchesAvailable(game.Grid);
        }

        private bool TileMatchesAvailable(Grid grid)
        {
            for (var x = 0; x < grid.Size; x++)
            {
                for (var y = 0; y < grid.Size; y++)
                {
                    var tile = grid.CellContent(new Position { X = x, Y = y });

                    if (tile != null)
                    {
                        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
                        {
                            var vector = GetVector(direction);
                            var cell = new Position { X = x + vector.X, Y = y + vector.Y };

                            var other = grid.CellContent(cell);

                            if (other != null && other.Value == tile.Value)
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }
    }
}
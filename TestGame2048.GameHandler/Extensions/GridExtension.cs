﻿using System.Collections.Generic;
using System.Linq;
using TestGame2048.GameHandler.Models;
using TestGame2048.Models.DomainObjects;

namespace TestGame2048.GameHandler.Extensions
{
    public static class GridExtension
    {
        public static FarthestPosition FindFarthestPosition(this Grid grid, Position cell, Position vector)
        {
            Position previous;

            do
            {
                previous = cell;
                cell = new Position { X = previous.X + vector.X, Y = previous.Y + vector.Y };
            } while (grid.WithinBounds(cell) &&
                     !grid.CellOccupied(cell));

            return new FarthestPosition
            {
                Farthest = previous,
                Next = cell
            };
        }

        public static bool WithinBounds(this Grid grid, Position position)
        {
            return position.X >= 0 && position.X < grid.Size &&
                   position.Y >= 0 && position.Y < grid.Size;
        }

        public static Tile CellContent(this Grid grid, Position position)
        {
            if (position.X >= grid.Size || position.Y>=grid.Size ||
                position.X < default(int) || position.Y < default(int))
            {
                return null;
            }
            return grid.Tiles[position.X, position.Y];
        }

        public static bool CellOccupied(this Grid grid, Position position)
        {
            return grid.CellContent(position) != null;
        }

        public static List<Position> AvailableCells(this Grid grid)
        {
            var cells = new List<Position>();
            for (var x = 0; x < grid.Size; x++)
            {
                for (var y = 0; y < grid.Size; y++)
                {
                    if (grid.Tiles[x,y]==null)
                    {
                        cells.Add(new Position{X = x,Y=y});
                    }
                }
            }
            return cells;
        }


        public static void PrepareTiles(this Grid grid)
        {
            for (var x = 0; x < grid.Size; x++)
            {
                for (var y = 0; y < grid.Size; y++)
                {
                    var tile = grid.CellContent(new Position { X = x, Y = y });

                    if (tile != null)
                    {
                        tile.MergedFrom = null;
                    }
                }
            }
        }
    }
}